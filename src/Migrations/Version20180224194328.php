<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180224194328 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE especialidad (id_especialidad INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id_especialidad)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE materias (id_materias INT AUTO_INCREMENT NOT NULL, fk_especialidad INT DEFAULT NULL, name VARCHAR(100) NOT NULL, credits VARCHAR(100) NOT NULL, INDEX IDX_F1B678608D6D66E0 (fk_especialidad), PRIMARY KEY(id_materias)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE materias ADD CONSTRAINT FK_F1B678608D6D66E0 FOREIGN KEY (fk_especialidad) REFERENCES especialidad (id_especialidad)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE materias DROP FOREIGN KEY FK_F1B678608D6D66E0');
        $this->addSql('DROP TABLE especialidad');
        $this->addSql('DROP TABLE materias');
    }
}
