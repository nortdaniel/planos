<?php
/**
 * Created by PhpStorm.
 * User: renecastillo
 * Date: 24/02/18
 * Time: 16:31
 */

namespace App\Form;

use App\Entity\Especialidad;
use App\Entity\Materias;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class MateriasType
 */
class MateriasType extends AbstractType
{
    /**
     * @inheritdoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Nombre',
                'required' => true,
            ])
            ->add('credits', TextType::class, [
                    'label' => 'Creditos',
                    'required' => true,
            ])
            ->add('fkSpeciality', EntityType::class, [
                'class' => Especialidad::class,
                'label' => 'Especialidad',
                'choice_label' => 'name',
                'required' => true,
            ])
            ->add('submit', SubmitType::class, [
                'label' =>  'Crear materia'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Materias::class,
        ]);
    }

}