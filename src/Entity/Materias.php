<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="materias")
 * @ORM\Entity(repositoryClass="App\Repository\MateriasRepository")
 */
class Materias
{
    /**
     * @ORM\Column(name="id_materias", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /***
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank()
     */
    private $credits;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Especialidad")
     * @ORM\JoinColumn(name="fk_especialidad", referencedColumnName="id_especialidad")
     * @Assert\NotBlank()
     */
    private $fkSpeciality;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Materias
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getCredits()
    {
        return $this->credits;
    }

    /**
     * @param string $credits
     *
     * @return Materias
     */
    public function setCredits($credits)
    {
        $this->credits = $credits;

        return $this;
    }

    /**
     * @return Especialidad
     */
    public function getFkSpeciality()
    {
        return $this->fkSpeciality;
    }

    /**
     * @param Especialidad $fkSpeciality
     *
     * @return $this
     */
    public function setFkSpeciality($fkSpeciality)
    {
        $this->fkSpeciality = $fkSpeciality;

        return $this;
    }
}

