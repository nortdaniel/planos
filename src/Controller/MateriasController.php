<?php

namespace App\Controller;

use App\Entity\Especialidad;
use App\Entity\Materias;
use App\Form\MateriasType;
use App\Repository\MateriasRepository;
use Doctrine\DBAL\Types\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Tests\Extension\Core\Type\SubmitTypeTest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class MateriasController
 */
class MateriasController extends Controller
{
    /**
     * @Route("/materias", name="materias")
     */
    public function index()
    {
        // you can fetch the EntityManager via $this->getDoctrine()
        // or you can add an argument to your action: index(EntityManagerInterface $em)
        $em = $this->getDoctrine()->getManager();

        /** @var Especialidad $especiallity */
        $especiallity = new Especialidad();
        $especiallity->setName('Inteligencia artificial');

        $matter = new Materias();
        $matter->setName('Redes neuronales');
        $matter->setCredits('Juan Carlos Martinez');
        $matter->setFkSpeciality($especiallity);

        // tell Doctrine you want to (eventually) save the Product (no queries yet)
        $em->persist($especiallity);
        $em->persist($matter);

        // actually executes the queries (i.e. the INSERT query)
        $em->flush();

        return new Response('Materia guardada ' . $matter->getId());
    }

    /**
     * @param int $id
     * @Route("/materia/{id}", name="materia_show")
     *
     * @return Response
     */
    public function showAction($id)
    {
        $materias = $this->getDoctrine()
            ->getRepository(Materias::class)
            ->find($id);

        if (!$materias) {
            throw $this->createNotFoundException(
                'Materia no encontrada id '.$id
            );
        }

        return new Response('Materia: '.$materias->getName());

        // or render a template
        // in the template, print things with {{ materias.name }}
        // return $this->render('materias/show.html.twig', ['materias' => $materias]);
    }

    /**
     * @param $id
     * @Route("/materia/update/{id}")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function updateAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var Materias $materia */
        $materia = $em->getRepository(Materias::class)->find($id);

        if (!$materia) {
            throw $this->createNotFoundException(
                'Materia no encontrada id '.$id
            );
        }

        $materia
            ->setName("Magento")
            ->setCredits("Rene castillo");

        $em->flush();

        return $this->redirectToRoute('materia_show', [
            'id' => $materia->getId()
        ]);
    }


    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var Materias $materia */
        $materia = $em->getRepository(Materias::class)->find($id);

        if (!$materia) {
            throw $this->createNotFoundException(
                'Materia no encontrada id '.$id
            );
        }

        $em->remove($materia);
        $em->flush();

        return $this->redirectToRoute('materia_show', [
            'id' => 1
        ]);
    }

    /**
     * @Route("/materias/new")
     * @param Request $request
     *
     * @return Response
     */
    public function newAction(Request $request)
    {
        /** @var Materias $materia */
        $materia = new Materias();

        $form = $this->createForm(MateriasType::class, $materia);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Materias $materia */
            $materia = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($materia);
            $em->flush();

            return $this->redirectToRoute('materia_show'. [
                    'id' => $materia->getId()
                ]
            );
        }

        return $this->render('default/new.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
