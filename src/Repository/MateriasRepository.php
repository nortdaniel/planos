<?php

namespace App\Repository;

use App\Entity\Materias;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Materias|null find($id, $lockMode = null, $lockVersion = null)
 * @method Materias|null findOneBy(array $criteria, array $orderBy = null)
 * @method Materias[]    findAll()
 * @method Materias[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MateriasRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Materias::class);
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('m')
            ->where('m.something = :value')->setParameter('value', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
